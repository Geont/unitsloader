unit SettingsFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TULSettingsFrame = class(TFrame)
    fodSelectDirectory: TFileOpenDialog;
    Label1: TLabel;
    edLibraryDirectory: TEdit;
    Select: TButton;
    chbUseSubfolders: TCheckBox;
    chbCacheSystem: TCheckBox;
    butRefreshCache: TButton;
    procedure SelectClick(Sender: TObject);
    procedure butRefreshCacheClick(Sender: TObject);
    procedure chbCacheSystemClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses
  ToolsAPI, UnitSearch;

{$R *.dfm}

procedure TULSettingsFrame.butRefreshCacheClick(Sender: TObject);
var
  lWatcher: IulIDEWatcher;
begin
  if Supports(IDEWatcher, IulIDEWatcher, lWatcher) then lWatcher.CacheSystem;
end;

procedure TULSettingsFrame.chbCacheSystemClick(Sender: TObject);
begin
  butRefreshCache.Enabled := chbCacheSystem.Checked;
end;

procedure TULSettingsFrame.SelectClick(Sender: TObject);
var
  lWatcher: IulIDEWatcher;
begin
  if fodSelectDirectory.Execute then
    edLibraryDirectory.Text := fodSelectDirectory.FileName;
end;

end.
