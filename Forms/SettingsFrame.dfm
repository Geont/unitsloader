object ULSettingsFrame: TULSettingsFrame
  Left = 0
  Top = 0
  Width = 557
  Height = 304
  Align = alClient
  TabOrder = 0
  ExplicitWidth = 451
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 79
    Height = 13
    Caption = 'Library directory'
  end
  object edLibraryDirectory: TEdit
    Left = 101
    Top = 13
    Width = 244
    Height = 21
    TabOrder = 0
  end
  object Select: TButton
    Left = 351
    Top = 11
    Width = 75
    Height = 25
    Caption = 'Select'
    TabOrder = 1
    OnClick = SelectClick
  end
  object chbUseSubfolders: TCheckBox
    Left = 101
    Top = 40
    Width = 97
    Height = 17
    Caption = 'Use subfolders'
    TabOrder = 2
  end
  object chbCacheSystem: TCheckBox
    Left = 101
    Top = 79
    Width = 121
    Height = 17
    Caption = 'Cache system paths'
    TabOrder = 3
    OnClick = chbCacheSystemClick
  end
  object butRefreshCache: TButton
    Left = 16
    Top = 75
    Width = 79
    Height = 25
    Caption = 'Refresh cache'
    Enabled = False
    TabOrder = 4
    OnClick = butRefreshCacheClick
  end
  object fodSelectDirectory: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <>
    OkButtonLabel = 'Select'
    Options = [fdoPickFolders]
    Left = 464
    Top = 8
  end
end
