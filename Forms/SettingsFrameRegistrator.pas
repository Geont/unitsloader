unit SettingsFrameRegistrator;

interface

uses
  ToolsAPI,
  SettingsFrame,
  VCL.Forms,
  UnitSearch
  ;

type
  TSettingsFrameRegistrator = class(TInterfacedObject, INTAAddInOptions)
  private
    fFrame: TULSettingsFrame;
    fConfigurator: TulConfigurator;
  public
    procedure AfterConstruction; override;

    procedure DialogClosed(Accepted: Boolean);
    procedure FrameCreated(AFrame: TCustomFrame);
    function GetArea: string;
    function GetCaption: string;
    function GetFrameClass: TCustomFrameClass;
    function GetHelpContext: Integer;
    function IncludeInIDEInsight: Boolean;
    function ValidateContents: Boolean;
  end;
  
implementation

  uses
    System.IOUtils,
    System.SysUtils;

{ TSettingsFrameRegistrator }

procedure TSettingsFrameRegistrator.AfterConstruction;
begin
  inherited;
  fConfigurator := TulConfigurator.Create;
end;

procedure TSettingsFrameRegistrator.DialogClosed(Accepted: Boolean);
var
  lWatcher: IulIDEWatcher;
begin
  if Accepted and Supports(IDEWatcher, IulIDEWatcher,lWatcher) then begin
    lWatcher.Config.LibraryDirectory := fFrame.edLibraryDirectory.Text;
    lWatcher.Config.UseSubfolder := fFrame.chbUseSubfolders.Checked;
    lWatcher.Config.CacheSystemFolders := fFrame.chbCacheSystem.Checked;
    fConfigurator.Save;
  end;
end;

procedure TSettingsFrameRegistrator.FrameCreated(AFrame: TCustomFrame);
var
  lWatcher: IulIDEWatcher;
begin
  if AFrame is TULSettingsFrame then begin
    fFrame := TULSettingsFrame(aFrame);
    if Supports(IDEWatcher, IulIDEWatcher,lWatcher) then begin
      fConfigurator.Load;
      fFrame.edLibraryDirectory.Text := lWatcher.Config.LibraryDirectory;
      fFrame.chbUseSubfolders.Checked := lWatcher.Config.UseSubfolder;
      fFrame.chbCacheSystem.Checked := lWatcher.Config.CacheSystemFolders;
    end;
  end;
end;

function TSettingsFrameRegistrator.GetArea: string;
begin
  Result := '';
end;

function TSettingsFrameRegistrator.GetCaption: string;
begin
  Result := 'UnitsLoader';
end;

function TSettingsFrameRegistrator.GetFrameClass: TCustomFrameClass;
begin
  Result := TULSettingsFrame;
end;

function TSettingsFrameRegistrator.GetHelpContext: Integer;
begin
  Result := 0;
end;

function TSettingsFrameRegistrator.IncludeInIDEInsight: Boolean;
begin
  Result := True;
end;

function TSettingsFrameRegistrator.ValidateContents: Boolean;
begin
  Result := TDirectory.Exists(TULSettingsFrame(fFrame).edLibraryDirectory.Text);
end;

var
  UnitLoaderSettings: INTAAddInOptions;

initialization
  UnitLoaderSettings := TSettingsFrameRegistrator.Create;
  (BorlandIDEServices as INTAEnvironmentOptionsServices).RegisterAddInOptions(UnitLoaderSettings);
finalization
  (BorlandIDEServices as INTAEnvironmentOptionsServices).UnregisterAddInOptions(UnitLoaderSettings);
  UnitLoaderSettings := nil;
end.
