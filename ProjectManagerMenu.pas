unit ProjectManagerMenu;

interface

uses
  ToolsAPI, System.Classes;

type

  TProjMenuOnExecute = procedure (const MenuContextList: IInterfaceList) of object;

  IProjectManagerMenu = interface
    ['{B199D1B4-D11B-4640-902A-30A2875C8283}']
    procedure SetOnExecute(const Event: TProjMenuOnExecute);
    function GetOnExecute: TProjMenuOnExecute;

    property OnExecute: TProjMenuOnExecute read GetOnExecute write SetOnExecute;
  end;

  TWatcherProjectManagerMenu = class(TNotifierObject, IOTANotifier, IOTALocalMenu, IOTAProjectManagerMenu)
  private
    fChecked: Boolean;
    fCaption: string;
    fName: string;
    fEnabled: Boolean;
    fHelpContext: Integer;
    fParent: string;
    fPosition: Integer;
    fVerb: string;
    fMultiSelectable: Boolean;

    fOnExecute: TProjMenuOnExecute;
  public
    procedure AfterConstruction; override;

    procedure Execute(const MenuContextList: IInterfaceList);
    function GetIsMultiSelectable: Boolean;
    function PostExecute(const MenuContextList: IInterfaceList): Boolean;
    function PreExecute(const MenuContextList: IInterfaceList): Boolean;
    procedure SetIsMultiSelectable(Value: Boolean);
    function GetCaption: string;
    function GetChecked: Boolean;
    function GetEnabled: Boolean;
    function GetHelpContext: Integer;
    function GetName: string;
    function GetParent: string;
    function GetPosition: Integer;
    function GetVerb: string;
    function GetOnExecute: TProjMenuOnExecute;
    procedure SetCaption(const Value: string);
    procedure SetChecked(Value: Boolean);
    procedure SetEnabled(Value: Boolean);
    procedure SetHelpContext(Value: Integer);
    procedure SetName(const Value: string);
    procedure SetParent(const Value: string);
    procedure SetPosition(Value: Integer);
    procedure SetVerb(const Value: string);
    procedure SetOnExecute(const Event: TProjMenuOnExecute);

    property Caption: string read GetCaption write SetCaption;
    property Checked: Boolean read GetChecked write SetChecked;
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property HelpContext: Integer read GetHelpContext write SetHelpContext;
    property Name: string read GetName write SetName;
    property Parent: string read GetParent write SetParent;
    property Position: Integer read GetPosition write SetPosition;
    property Verb: string read GetVerb write SetVerb;
    property IsMultiSelectable: Boolean read GetIsMultiSelectable write SetIsMultiSelectable;
    property OnExecute: TProjMenuOnExecute read GetOnExecute write SetOnExecute;
  end;

implementation
  {$region 'TWatcherProjectMenuItem'}

procedure TWatcherProjectManagerMenu.AfterConstruction;
begin
  inherited;
  fCaption := 'asfsad';
  fChecked := False;
  fName := 'MenuItem';
  fEnabled := True;
  fHelpContext := 1;
  fParent := '';
  fPosition := pmmpUserOptions;
  fVerb := 'Verb';
  fMultiSelectable := True;
end;

procedure TWatcherProjectManagerMenu.Execute(
  const MenuContextList: IInterfaceList);
begin
  if Assigned(fOnExecute) then
    OnExecute(MenuContextList);
end;

function TWatcherProjectManagerMenu.GetCaption: string;
begin
  Result := fCaption;
end;

function TWatcherProjectManagerMenu.GetChecked: Boolean;
begin
  Result := fChecked;
end;

function TWatcherProjectManagerMenu.GetEnabled: Boolean;
begin
  Result := fEnabled;
end;

function TWatcherProjectManagerMenu.GetHelpContext: Integer;
begin
  Result := fHelpContext;
end;

function TWatcherProjectManagerMenu.GetIsMultiSelectable: Boolean;
begin
  Result := fMultiSelectable;
end;

function TWatcherProjectManagerMenu.GetName: string;
begin
  Result := fName;
end;

function TWatcherProjectManagerMenu.GetOnExecute: TProjMenuOnExecute;
begin
  Result := fOnExecute;
end;

function TWatcherProjectManagerMenu.GetParent: string;
begin
  Result := fParent;
end;

function TWatcherProjectManagerMenu.GetPosition: Integer;
begin
  Result := fPosition;
end;

function TWatcherProjectManagerMenu.GetVerb: string;
begin
  Result := fVerb;
end;

function TWatcherProjectManagerMenu.PostExecute(
  const MenuContextList: IInterfaceList): Boolean;
begin
  Result := True;
end;

function TWatcherProjectManagerMenu.PreExecute(
  const MenuContextList: IInterfaceList): Boolean;
begin
  Result := True;
end;

procedure TWatcherProjectManagerMenu.SetCaption(const Value: string);
begin
  fCaption := Value;
end;

procedure TWatcherProjectManagerMenu.SetChecked(Value: Boolean);
begin
  fChecked := Value;
end;

procedure TWatcherProjectManagerMenu.SetEnabled(Value: Boolean);
begin
  fEnabled := Value;
end;

procedure TWatcherProjectManagerMenu.SetHelpContext(Value: Integer);
begin
  fHelpContext := Value;
end;

procedure TWatcherProjectManagerMenu.SetIsMultiSelectable(Value: Boolean);
begin
  fMultiSelectable := Value;
end;

procedure TWatcherProjectManagerMenu.SetName(const Value: string);
begin
  fName := Value;
end;

procedure TWatcherProjectManagerMenu.SetOnExecute(
  const Event: TProjMenuOnExecute);
begin
  fOnExecute := Event;
end;

procedure TWatcherProjectManagerMenu.SetParent(const Value: string);
begin
  fParent := Value;
end;

procedure TWatcherProjectManagerMenu.SetPosition(Value: Integer);
begin
  fPosition := Value;
end;

procedure TWatcherProjectManagerMenu.SetVerb(const Value: string);
begin
  fVerb := Value;
end;

{$endregion}

end.
