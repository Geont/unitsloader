unit EnvironmentHelper;

interface

uses
  System.Win.Registry,
  System.Generics.Collections,
  System.Classes
  ;


type
  TKnownPlatforms = (
    Win32,
    Win64,
    OSX32,
    iOSSimulator,
    iOSDevice,
    Android
  );
const

  {$region 'Definitions'}

  {$region 'BDS'}
  {$IFDEF VER280}
  BDS = 'Software\Embarcadero\BDS\15.0';
  {$ENDIF}
  {$IFDEF VER270}
  BDS = 'Software\Embarcadero\BDS\14.0';
  {$ENDIF}
  {$IFDEF VER260}
  BDS = 'Software\Embarcadero\BDS\12.0';
  {$ENDIF}
  {$endregion}

  {$region 'PLATFORM'}
  {$IFDEF Android}
  CURRENT_PLATFORM = Android;
  {$ENDIF}
  {$IFDEF MacOS32}
  CURRENT_PLATFORM = OSX32;
  {$ENDIF}
  {$IFDEF iOS}
  {$IFDEF CPUARM}
  CURRENT_PLATFORM = iOSDevice;
  {$ELSE}
  CURRENT_PLATFORM = iOSSimulator;
  {$ENDIF}
  {$ENDIF}
  {$IFDEF WIN32}
  CURRENT_PLATFORM = Win32;
  {$ENDIF}
  {$IFDEF WIN64}
  CURRENT_PLATFORM = Win64;
  {$ENDIF}
  {$endregion}

  {$endregion}

  TLibraryPlatforms: array [Low(TKnownPlatforms)..High(TKnownPlatforms)] of string = ('Win32','Win64','OSX32',
  'iOSSimulator', 'iOSDevice','Android32');
  TOTAPlatforms: array [Low(TKnownPlatforms)..High(TKnownPlatforms)] of string = ('Win32','Win64','OSX32',
  'iOSSimulator', 'iOSDevice','Android');
  function FindPlatform(const aName: string): TKnownPlatforms;

type
  /// <summary>
  ///  Using <c>SetPlatform</c> we can change which platform is used for. If not specified it's used currently selected.
  /// </summary>
  IEnvironmentService = interface
    ['{D332A755-C5BF-4C81-81FC-59FE77115BA5}']
    /// <summary>
    ///   For selection of platform from which we retrieve informations.
    /// </summary>
    procedure SetPlatform(const aPlatform: TKnownPlatforms); overload;
    procedure SetPlatform(const aPlatform: string); overload;
    function GetLibraryPath: TStrings;
    procedure SetLibraryPath(const aPath: TStrings);
    function GetBrowsingPath: TStrings;
    procedure SetBrowsingPath(const aPath: TStrings);
    function GetUnitScopes: TStrings;
    procedure SetUnitScopes(const aScopes: TStrings);
    /// <summary>
    ///   It will save into registry contains of current
    /// </summary>
    procedure SavePaths;
    //function GetEnvironmentVariable: TStrings;
    function ParseEnvironmentVariables(const Value: TStrings): TStrings;

    property LibraryPath: TStrings read GetLibraryPath write SetLibraryPath;
    property BrowsingPath: TStrings read GetBrowsingPath write SetBrowsingPath;
    property UnitScopes: TStrings read GetUnitScopes write SetUnitScopes;
  end;

  TEnvironmentHelper = class(TInterfacedObject, IEnvironmentService)
  private type
    TNames = record
      LibraryPath: string;
      BrowsingPath: string;
      DebugDCUPath: string;
      UnitScopes: string;
    end;
  private
    fLibraryKey: string;
    /// <summary>
    ///   Environment variables key name
    /// </summary>
    fEnvVarKey: string;
    fRegistry: TRegistry;
    fRegistryNames: TNames;
    fLibraryPath: TStrings;
    fBrowsingPath: TStrings;
    fUnitScopes: TStrings;
    fPlatform: TKnownPlatforms;
    fRegistryProcessed: Boolean;
    procedure ReadFromRegistry;
    procedure ProccessRegistry;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;

    function ParseEnvironmentVariables(const Value: TStrings): TStrings;
    procedure SetPlatform(const aPlatform: TKnownPlatforms); overload;
    procedure SetPlatform(const aPlatform: string); overload;
    function GetLibraryPath: TStrings;
    procedure SetLibraryPath(const aPath: TStrings);
    function GetBrowsingPath: TStrings;
    procedure SetBrowsingPath(const aPath: TStrings);
    function GetUnitScopes: TStrings;
    procedure SetUnitScopes(const aScopes: TStrings);
    procedure SavePaths;
  end;

implementation

uses
  System.StrUtils
  , System.SysUtils
  , Winapi.Windows
  , System.RegularExpressions
  ;

function FindPlatform(const aName: string): TKnownPlatforms;
var
  I: Integer;
begin
  for I := 0 to Integer(High(TOTAPlatforms)) do
    if SameStr(TOTAPlatforms[TKnownPlatforms(I)], aName) then begin
      Result := TKnownPlatforms(I);
      Break;
    end;
end;

{ TEnvironmentHelper }

procedure TEnvironmentHelper.AfterConstruction;
var
  lText: string;
  I: Integer;
begin
  inherited;
  fLibraryPath := TStringList.Create;
  fLibraryPath.Delimiter := ';';
  fLibraryPath.StrictDelimiter := True;
  fBrowsingPath := TStringList.Create;
  fBrowsingPath.Delimiter := ';';
  fBrowsingPath.StrictDelimiter := True;
  fUnitScopes := TStringList.Create;
  fUnitScopes.Delimiter := ';';
  fUnitScopes.StrictDelimiter := True;
  fPlatform := CURRENT_PLATFORM;
  fRegistryNames.LibraryPath := 'Search Path';
  fRegistryNames.BrowsingPath := 'Browsing Path';
  fRegistryNames.UnitScopes := 'Unit Scopes';
  fRegistry := TRegistry.Create;
  fRegistry.RootKey := HKEY_CURRENT_USER;
  ReadFromRegistry;
end;

procedure TEnvironmentHelper.BeforeDestruction;
begin
  fRegistry.Free;
  inherited;
end;

function TEnvironmentHelper.GetBrowsingPath: TStrings;
begin
  ProccessRegistry;
  Result := fBrowsingPath;//.DelimitedText;
end;

function TEnvironmentHelper.GetLibraryPath: TStrings;
begin
  ProccessRegistry;
  Result := fLibraryPath;//.DelimitedText;
end;

function TEnvironmentHelper.GetUnitScopes: TStrings;
begin
  ProccessRegistry;
  Result := fUnitScopes;
end;

function TEnvironmentHelper.ParseEnvironmentVariables(
  const Value: TStrings): TStrings;
var
  I: Integer;
  lRegExp: TRegEx;
  lSimpleMatch: TMatch;
  lMatch: TMatchCollection;
  lMatchStr, lEnvVarName: string;
  lEnvVarValue, lNewStr: string;
begin
  // ^\$\((.*?)\)$
  lRegExp := TRegEx.Create('\$\((.*?\))',[TRegExOption.roIgnoreCase, roMultiLine]);

  Result := TStringList.Create;
  for I := 0 to Value.Count - 1 do begin
    lMatch := lRegExp.Matches(Value.Strings[I]);
    lNewStr := Value.Strings[I];
    for lSimpleMatch in lMatch do
      if lSimpleMatch.Success then begin
        lMatchStr := lSimpleMatch.Value;
        lEnvVarName := lMatchStr.Replace('$','');
        lEnvVarName := lEnvVarName.Replace('(','');
        lEnvVarName := lEnvVarName.Replace(')','');
        lEnvVarValue := GetEnvironmentVariable(lEnvVarName);
        lNewStr := lNewStr.Replace(lMatchStr, lEnvVarValue);
      end;
    Result.Add(lNewStr);
  end;
end;

procedure TEnvironmentHelper.ProccessRegistry;
begin
  if not fRegistryProcessed then
    ReadFromRegistry;
  fRegistryProcessed := True;
end;

procedure TEnvironmentHelper.ReadFromRegistry;
var
  lKey, path: string;
begin
  if fRegistry.KeyExists(BDS) then begin
    lKey := IncludeTrailingPathDelimiter(BDS)+ 'Library\'+TLibraryPlatforms[fPlatform];
    if fRegistry.OpenKey(lKey, false) then begin
      path := fRegistry.ReadString(fRegistryNames.LibraryPath);
      fLibraryPath.DelimitedText := path;
      path := fRegistry.ReadString(fRegistryNames.BrowsingPath);
      fBrowsingPath.DelimitedText := path;
      path := fRegistry.ReadString(fRegistryNames.UnitScopes);
      fUnitScopes.DelimitedText := path;
      fRegistry.CloseKey;
    end;
  end;
end;

procedure TEnvironmentHelper.SavePaths;
var
  lKey: string;
begin
  if fRegistry.KeyExists(BDS) then begin
    lKey := IncludeTrailingPathDelimiter(BDS)+ 'Library\'+TLibraryPlatforms[fPlatform];
    if fRegistry.OpenKey(lKey, false) then begin
      fRegistry.WriteString(fRegistryNames.LibraryPath, fLibraryPath.DelimitedText);
      fRegistry.WriteString(fRegistryNames.BrowsingPath, fBrowsingPath.DelimitedText);
      fRegistry.WriteString(fRegistryNames.UnitScopes, fUnitScopes.DelimitedText);
      fRegistry.CloseKey;
    end;
  end;

end;

procedure TEnvironmentHelper.SetBrowsingPath(const aPath: TStrings);
begin
  fBrowsingPath.Assign(aPath);
end;

procedure TEnvironmentHelper.SetLibraryPath(const aPath: TStrings);
begin
  fLibraryPath.Assign(aPath);
end;

procedure TEnvironmentHelper.SetPlatform(const aPlatform: string);
var
  I: Integer;
begin
  for I := 0 to Ord(TKnownPlatforms.Android) do
    if TLibraryPlatforms[TKnownPlatforms(I)].ToLower = aPlatform.ToLower then begin
      SetPlatform(TKnownPlatforms(I));
      Break;
    end;
end;

procedure TEnvironmentHelper.SetUnitScopes(const aScopes: TStrings);
begin
  fUnitScopes.Assign(aScopes);
end;

procedure TEnvironmentHelper.SetPlatform(const aPlatform: TKnownPlatforms);
begin
  fPlatform := aPlatform;
  ReadFromRegistry;
end;

end.
