unit UnitSearch;

interface

uses
  ToolsAPI,
  ulSimplePasPar,
  ulPasLexTypes,
  System.Generics.Collections,
  System.Classes,
  System.SysUtils,
  EnvironmentHelper,
  VCL.Menus,
  ProjectManagerMenu
  ;

type

  TCacheDict = TDictionary<string, string>;

  TulConfig = class(TPersistent)
  private
    fLibraryDirectory: string;
    fUseSubfolder: Boolean;
    fCacheSystemFolders: Boolean;
  public
    procedure BeforeDestruction; override;

    property LibraryDirectory: string read FLibraryDirectory write FLibraryDirectory;
    property UseSubfolder: Boolean read FUseSubfolder write FUseSubfolder;
    property CacheSystemFolders: Boolean read FCacheSystemFolders write FCacheSystemFolders;
  end;

  IulProjectWatcher = interface
  ['{7C0D5235-3155-4E8C-A81C-C59AD72FD50C}']
    procedure SetSystemCache(const ACache: TDictionary<TKnownPlatforms, TCacheDict>);
    function GetSystemCache: TDictionary<TKnownPlatforms, TCacheDict>;
    procedure SetConfig(const AConfig: TulConfig);
    function GetConfig: TulConfig;

    function RegisterToProject(const aProject: IOTAProject): Boolean;
    procedure UnregisterFromProject;

    property SystemCache: TDictionary<TKnownPlatforms, TCacheDict> read GetSystemCache write SetSystemCache;
    property Config: TulConfig read GetConfig write SetConfig;
  end;

  IulIDEWatcher = interface
    ['{176AA8C6-0AE7-4C71-ADC3-51671D5808EE}']
    procedure SetConfig(const AConfig: TulConfig);
    function GetConfig: TulConfig;
    procedure CacheSystem;

    property Config: TulConfig read GetConfig write SetConfig;
  end;

  TulConfigurator = class
  public
    procedure Save;
    procedure Load;

  end;

  TUnitSearch = class(TmwSimplePasPar)
  private
    fUnits: TList<string>;
    procedure ErrorOccurred(Sender: TObject; const Typ : TMessageEventType; const Msg: string; X, Y: Integer );
  protected
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;

    procedure ClearUnits;
    function FoundUnits: TArray<string>;
    procedure UsedUnitName; override;
  end;

  TulProjectWatcher = class(TModuleNotifierObject, IOTANotifier, IOTAProjectNotifier, IOTAModuleNotifier, IulProjectWatcher)
  private
    FSystemCache: TDictionary<TKnownPlatforms, TCacheDict>;
    FProject: IOTAProject;
    FIndex: Integer;
    FSearch: TUnitSearch;
    fEnvHelper: IEnvironmentService;
    FConfig: TulConfig;
    function TryAddUnitFromLibrary(const aUnitName: string): Boolean;
    /// <summary>
    ///   Search through <c>aStrings</c> for <c>aValue</c>
    /// </summary>
    function FindInPaths(const aStrings: TStrings; const aValue: string): Boolean;
    function FindUnit(const aPlatform: string; const aValue: string): Boolean;
  protected
    procedure SetSystemCache(const ACache: TDictionary<TKnownPlatforms, TCacheDict>);
    function GetSystemCache: TDictionary<TKnownPlatforms, TCacheDict>;
    procedure SetConfig(const AConfig: TulConfig);
    function GetConfig: TulConfig;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;

    procedure ModuleRenamed(const NewName: string); overload;
    procedure ModuleAdded(const AFileName: string);
    procedure ModuleRemoved(const AFileName: string);
    procedure ModuleRenamed(const AOldFileName, ANewFileName: string); overload;
    function CheckOverwrite: Boolean;

    function RegisterToProject(const aProject: IOTAProject): Boolean;
    procedure UnregisterFromProject;
    property SystemCache: TDictionary<TKnownPlatforms, TCacheDict> read FSystemCache write FSystemCache;
    property Config: TulConfig read FConfig write FConfig;
  end;

  TulIDEWatcher = class(TNotifierObject, IOTANotifier, IOTAIDENotifier, IOTAProjectMenuItemCreatorNotifier, IulIDEWatcher)
  private
    FSystemCache: TDictionary<TKnownPlatforms, TCacheDict>;
    FWatchedProjects: TDictionary<IOTAProject, IOTAProjectNotifier>;
    FGroupLoaded: Boolean;
    FConfig: TulConfig;
    fAddedWatcher: Boolean;
    procedure DistributeConfig;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure CleanWatched;
    procedure CacheSystem;

    procedure FileNotification(NotifyCode: TOTAFileNotification;
      const FileName: string; var Cancel: Boolean);
    procedure BeforeCompile(const Project: IOTAProject; var Cancel: Boolean); overload;
    procedure AfterCompile(Succeeded: Boolean); overload;

    procedure AddMenu(const Project: IOTAProject; const IdentList: TStrings;
      const ProjectManagerMenuList: IInterfaceList; IsMultiSelect: Boolean);
    procedure MenuExecute(const MenuContextList: IInterfaceList);

    procedure SetConfig(const AConfig: TulConfig);
    function GetConfig: TulConfig;

    property Config: TulConfig read FConfig write FConfig;
  end;
  
  procedure Register;

var
  IDEWatcher: IOTAIDENotifier;

implementation

uses
  System.StrUtils,
  System.IOUtils,
  System.Types,
  System.IniFiles,
  Windows,
  PlatformAPI
  ;

var
  Index, Index2: Integer;

procedure Register;
begin
  IDEWatcher := TulIDEWatcher.Create;
  Index := (BorlandIDEServices as IOTAServices).AddNotifier(IDEWatcher);
  Index2 := (BorlandIDEServices as IOTAProjectManager).AddMenuItemCreatorNotifier(TulIDEWatcher(IDEWatcher));
end;

{$region 'IDE watcher'}

procedure TulIDEWatcher.AddMenu(const Project: IOTAProject;
  const IdentList: TStrings; const ProjectManagerMenuList: IInterfaceList;
  IsMultiSelect: Boolean);
var
  item: TWatcherProjectManagerMenu;
begin

  if (not IsMultiSelect) and(IdentList.IndexOf(sProjectContainer) <> -1)
   and Assigned(Project) then begin
    item := TWatcherProjectManagerMenu.Create;
    item.Enabled := True;
    item.Caption := 'UnitsLoader Active';
    item.Checked := FWatchedProjects.ContainsKey(Project);
    item.Name := 'vutsUnitLoaderActive'+ TPath.GetFileNameWithoutExtension(Project.FileName);
    item.Verb := 'UnitLoaderActive';
    item.IsMultiSelectable := False;
    item.Position := pmmpUserOptions;
    item.OnExecute := MenuExecute;
    ProjectManagerMenuList.Add(item);
  end;
end;

procedure TulIDEWatcher.AfterCompile(Succeeded: Boolean);
begin
  //do nothing
end;

procedure TulIDEWatcher.AfterConstruction;
var
  I: Integer;
  lWatcher: TulProjectWatcher;
  lProject: IOTAProject;
  lPlatforms : TArray<string>;
begin
  inherited;
  lPlatforms := (BorlandIDEServices as IOTAPlatformServices).AllPlatforms;
  FSystemCache := TDictionary<TKnownPlatforms, TCacheDict>.Create(Length(lPlatforms));
  for I := 0 to High(lPlatforms) do
    FSystemCache.Add(FindPlatform(lPlatforms[I]), TCacheDict.Create);
  FWatchedProjects := TDictionary<IOTAProject, IOTAProjectNotifier>.Create(5);
  FGroupLoaded := False;
  FConfig := TulConfig.Create;

//  if Assigned((BorlandIDEServices as IOTAModuleServices).MainProjectGroup) then begin
//    for I := 0 to (BorlandIDEServices as IOTAModuleServices).MainProjectGroup.ProjectCount - 1 do begin
//      lWatcher := TulProjectWatcher.Create;
//      lProject := (BorlandIDEServices as IOTAModuleServices).MainProjectGroup.Projects[I];
//      if lWatcher.RegisterToProject(lProject) then begin
//        fWatchedProjects.Add(lProject, lWatcher);
//        lWatcher.Config := FConfig;
//      end else
//        lWatcher.Free;
//    end;
//    FGroupLoaded := True;
//  end;

  CacheSystem;
  fAddedWatcher := False;
end;

procedure TulIDEWatcher.BeforeCompile(const Project: IOTAProject;
  var Cancel: Boolean);
begin
  Cancel := False;
end;

procedure TulIDEWatcher.BeforeDestruction;
begin
  CleanWatched;
  FSystemCache.Clear;
  FSystemCache.Free;
  FWatchedProjects.Free;
  inherited;
end;

procedure TulIDEWatcher.CacheSystem;
var
  lPlatforms: TArray<string>;
  lPaths: TStrings;
  lPath: string;
  lFiles: TStringDynArray;
  I: Integer;
  lEnvHelper: IEnvironmentService;
  J: Integer;
  lPlatform: TKnownPlatforms;
begin
  lPlatforms := (BorlandIDEServices as IOTAPlatformServices).AllPlatforms;
  lEnvHelper := TEnvironmentHelper.Create;
  for I := 0 to High(lPlatforms) do begin
    lEnvHelper.SetPlatform(lPlatforms[I]);
    lPlatform := FindPlatform(lPlatforms[I]);
    lPaths := lEnvHelper.LibraryPath;
    for lPath in lPaths do begin
      if TDirectory.Exists(lPath) then begin
        lFiles := TDirectory.GetFiles(lPath, '*.pas', TSearchOption.soTopDirectoryOnly);
        for J := 0 to High(lFiles) do begin
          if not FSystemCache[lPlatform].ContainsKey(TPath.GetFileNameWithoutExtension(lFiles[J])) then
            FSystemCache[lPlatform].Add(TPath.GetFileNameWithoutExtension(lFiles[J]), lFiles[J]);
        end;
        lFiles := TDirectory.GetFiles(lPath, '*.dcu', TSearchOption.soTopDirectoryOnly);
        for J := 0 to High(lFiles) do
          if not FSystemCache[lPlatform].ContainsKey(TPath.GetFileNameWithoutExtension(lFiles[J])) then
            FSystemCache[lPlatform].Add(TPath.GetFileNameWithoutExtension(lFiles[J]), lFiles[J]);
      end;
    end;
  end;
end;

procedure TulIDEWatcher.CleanWatched;
var
  I: Integer;
begin
  for I := 0 to FWatchedProjects.Count - 1 do begin
    TulProjectWatcher(FWatchedProjects.Values.ToArray[I]).UnregisterFromProject;
    FWatchedProjects.Values.ToArray[I] := nil;
  end;
  FWatchedProjects.Clear;
end;

procedure TulIDEWatcher.DistributeConfig;
var
  lProjectNotif: TPair<IOTAProject,IOTAProjectNotifier>;
begin
  for lProjectNotif in FWatchedProjects do begin
    if lProjectNotif.Value is TulProjectWatcher then
      TulProjectWatcher(lProjectNotif.Value).Config := FConfig;
  end;
end;

procedure TulIDEWatcher.FileNotification(NotifyCode: TOTAFileNotification;
  const FileName: string; var Cancel: Boolean);
var
  I: Integer;
  lWatcher: TulProjectWatcher;
  lProject: IOTAProject;

  function ContainsFileName(const aFileName: string): Boolean;
  var
    lKey: IOTAProject;
  begin
    Result := False;
    for lKey in FWatchedProjects.Keys do begin
      Result := SameStr(lKey.FileName, aFileName);
      if Result then Break;
    end;
  end;

  function FindProject(const aFileName: string): IOTAProject;
  var
    lKey: IOTAProject;
  begin
    Result := nil;
    for lKey in FWatchedProjects.Keys do begin
      if SameStr(lKey.FileName, aFileName) then begin  
        Result := lKey;
        Break;  
      end;
    end;
  end;
  
begin
  if (NotifyCode = ofnFileOpened) and (TPath.GetExtension(FileName)='.groupproj') then begin //after grouproject is opened we iterate through projects of main project group to
//    for I := 0 to (BorlandIDEServices as IOTAModuleServices).MainProjectGroup.ProjectCount - 1 do begin
//      lWatcher := TUnitsWatcher.Create;
//      lWatcher.SystemCache := FSystemCache;
//      lProject := (BorlandIDEServices as IOTAModuleServices).MainProjectGroup.Projects[I];
//      if lWatcher.RegisterToProject(lProject) then
//        fWatchedProjects.Add(lProject, lWatcher)
//      else
//        lWatcher.Free;
//    end;
    FGroupLoaded := True;
  end;
  if (NotifyCode = ofnFileClosing) and (TPath.GetExtension(FileName)='.groupproj') then
    FGroupLoaded := False;
//  if (NotifyCode = ofnFileOpened) and (TPath.GetExtension(FileName)='.dproj')
//  and FGroupLoaded and not ContainsFileName(FileName) then begin
//    lWatcher := TUnitsWatcher.Create;
//    lProject := (BorlandIDEServices as IOTAModuleServices).MainProjectGroup.FindProject(FileName);
//    if not Assigned(lProject) then
//      for I := 0 to (BorlandIDEServices as IOTAModuleServices).MainProjectGroup.ProjectCount - 1 do
//        if (BorlandIDEServices as IOTAModuleServices).MainProjectGroup.Projects[I].FileName = FileName then begin
//          lProject := (BorlandIDEServices as IOTAModuleServices).MainProjectGroup.Projects[I];
//          Break;
//        end;
//    if lWatcher.RegisterToProject(lProject) then
//      fWatchedProjects.Add(lProject, lWatcher)
//    else
//      lWatcher.Free;
//  end;
  if (NotifyCode = ofnFileClosing) and ContainsFileName(FileName) {and FGroupLoaded} then begin
    lProject := FindProject(FileName);
    if Assigned(lProject) then begin
      TulProjectWatcher(FWatchedProjects[lProject]).UnregisterFromProject;
      FWatchedProjects[lProject] := nil;
      FWatchedProjects.Remove(lProject);
    end;
  end;
  Cancel:= False;
end;

function TulIDEWatcher.GetConfig: TulConfig;
begin
  Result := FConfig;
end;

procedure TulIDEWatcher.MenuExecute(const MenuContextList: IInterfaceList);
var
  I: Integer;
  lItem: IOTAProjectMenuContext;
  lWatcher: IulProjectWatcher;
  lProjNot: IOTAProjectNotifier;
begin
  try
    for I := 0 to MenuContextList.Count - 1 do
      if Supports(MenuContextList.Items[I], IOTAMenuContext, lItem) then begin
        if Assigned(lItem.Project) then
          if FWatchedProjects.ContainsKey(lItem.Project) and
           Supports(FWatchedProjects[lItem.Project], IulProjectWatcher, lWatcher) then begin
            lWatcher.UnregisterFromProject;
            FWatchedProjects.Remove(lItem.Project);  
          end else begin
            lWatcher := TulProjectWatcher.Create;
            lWatcher.SystemCache := FSystemCache;
            lWatcher.Config := FConfig;
            lWatcher.RegisterToProject(lItem.Project);
            if Supports(lWatcher, IOTAProjectNotifier, lProjNot) then
              FWatchedProjects.Add(lItem.Project, lProjNot);
          end;
      end;
  finally
    
  end;
end;

procedure TulIDEWatcher.SetConfig(const AConfig: TulConfig);
begin
  FConfig := AConfig;
  DistributeConfig;
end;

{$endregion}

{$region 'Units watcher'}

procedure TulProjectWatcher.AfterConstruction;
begin
  inherited;
  FSearch := TUnitSearch.Create;
  FSearch.UseDefines := True;
  fEnvHelper := TEnvironmentHelper.Create;
end;

procedure TulProjectWatcher.BeforeDestruction;
begin
  FSearch.Free;
  inherited;
end;

function TulProjectWatcher.CheckOverwrite: Boolean;
begin
  Result := True;
end;

function TulProjectWatcher.FindInPaths(const aStrings: TStrings;
  const aValue: string): Boolean;
var
  path, filepath: string;
  paths: TStrings;
begin
  Result := False;
  paths := fEnvHelper.ParseEnvironmentVariables(aStrings);
  for path in paths do begin
    if TDirectory.Exists(path) then begin
      for filepath in TDirectory.GetFiles(path,aValue+'.pas', TSearchOption.soTopDirectoryOnly) do
        if TPath.GetFileNameWithoutExtension(filepath).ToLower = aValue.ToLower then begin
          Result := True;
          Break;
        end;
      if not Result then
        for filepath in TDirectory.GetFiles(path,aValue+'.dcu', TSearchOption.soTopDirectoryOnly) do
          if TPath.GetFileNameWithoutExtension(filepath).ToLower = aValue.ToLower then begin
            Result := True;
            Break;
          end;
    end;
    if Result then Break;
  end;
end;

function TulProjectWatcher.FindUnit(const aPlatform: string; const aValue: string): Boolean;
var
  lPlatform: TKnownPlatforms;
begin
  lPlatform := FindPlatform(aPlatform);
  Result := FSystemCache[lPlatform].ContainsKey(aValue);
end;

function TulProjectWatcher.GetConfig: TulConfig;
begin
  Result := FConfig;
end;

function TulProjectWatcher.GetSystemCache: TDictionary<TKnownPlatforms, TCacheDict>;
begin
  Result := FSystemCache;
end;

procedure TulProjectWatcher.ModuleAdded(const AFileName: string);
var
  lStringstream, lStringStream2: TStringStream;
  lStream: TMemoryStream;
  s, lPath: string;
  lAPath: AnsiString;
  lFoundUnits, lUnitScopes, lPlatforms:TArray<string>;
  lFiles: TStringList;
  lLibraryPaths, lBrowsingPaths: TStrings;
  lConfig: TOTAOptionNameArray;
  lOption: TOTAOptionName;
  I, J: Integer;
  lFilePath: string;
  lUnitFound: Boolean;
  lLibraryPath, lBrowsingPath: string;
  lPrjUnitScopes: IOTAProjectUnitScopes;
begin
  if (TPath.GetExtension(AFileName) = '.pas') and (TFile.Exists(AFileName)) then begin
    lStringstream := TStringStream.Create('', TEncoding.ANSI);
    try
      lStringstream.LoadFromFile(AFileName);
      s := lStringStream.DataString;
    finally
      lStringstream.Free;
    end;
    lStringstream2 := TStringStream.Create(s, TEncoding.Unicode);
    try
      lStringstream2.Position := 0;
      FSearch.ClearUnits;
      FSearch.Run(TPath.GetFileNameWithoutExtension(AFileName), lStringstream2);
    finally
      lStringStream2.Free;
    end;
    lFoundUnits := FSearch.FoundUnits;
    lFiles := TStringList.Create;
    FProject.GetCompleteFileList(lFiles);
    if Supports(FProject, IOTAProjectUnitScopes, lPrjUnitScopes) then
      lUnitScopes := lPrjUnitScopes.GetUnitScopes('', FProject.CurrentPlatform);

    fEnvHelper.SetPlatform(FProject.CurrentPlatform);

    for I := 0 to Length(lFoundUnits) - 1 do begin
      lUnitFound := False;
      if FConfig.CacheSystemFolders then lUnitFound := FindUnit(FProject.CurrentPlatform, lFoundUnits[I])
      else begin
        //we search inside project if there isn't already found unit
        for lFilePath in lFiles do
          if TPath.GetFileNameWithoutExtension(lFilePath).ToLower = lFoundUnits[I].ToLower then begin
            lUnitFound := True;
            Break;
          end;
        //we search in paths from library path
        if not lUnitFound then lUnitFound := FindInPaths(fEnvHelper.LibraryPath, lFoundUnits[I]);

        //we search in paths from browsing path
        if not lUnitFound then lUnitFound := FindInPaths(fEnvHelper.BrowsingPath, lFoundUnits[I]);

        //we search in library path but now we expand unit with names from unit scope
        if not lUnitFound then
          for J := 0 to High(lUnitScopes) do
            if FindInPaths(fEnvHelper.LibraryPath, lUnitScopes[I]+'.'+lFoundUnits[I]) then begin
              lUnitFound := True;
              break;
            end;
        if not lUnitFound then
          for J := 0 to High(lUnitScopes) do
            if FindInPaths(fEnvHelper.BrowsingPath, lUnitScopes[I]+'.'+lFoundUnits[I]) then begin
              lUnitFound := True;
              break;
            end;
      end;
      if not lUnitFound then
        lUnitFound := TryAddUnitFromLibrary(lFoundUnits[I]);
    end;
  end;
end;

procedure TulProjectWatcher.ModuleRemoved(const AFileName: string);
begin
  if AFileName.IsEmpty then
    raise Exception.Create('Filename is empty');
end;

procedure TulProjectWatcher.ModuleRenamed(const NewName: string);
begin
  if NewName.IsEmpty then
    raise Exception.Create('Filename is empty');
end;

procedure TulProjectWatcher.ModuleRenamed(const AOldFileName, ANewFileName: string);
begin
   if AOldFileName.IsEmpty then
    raise Exception.Create('Filename is empty');
end;

function TulProjectWatcher.RegisterToProject(const aProject: IOTAProject): Boolean;
begin
  if Assigned(aProject) then begin
    FProject := aProject;
    FIndex := FProject.AddNotifier(Self);
    Result := True;
  end else
    Result := False;
end;

procedure TulProjectWatcher.SetConfig(const AConfig: TulConfig);
begin
  FConfig := AConfig;
end;

procedure TulProjectWatcher.SetSystemCache(
  const ACache: TDictionary<TKnownPlatforms, TCacheDict>);
begin
  FSystemCache := ACache;
end;

function TulProjectWatcher.TryAddUnitFromLibrary(const aUnitName: string): Boolean;
var
  path, filepath: string;
  lFiles: TStringDynArray;
begin
  Result := False;
  path := FConfig.LibraryDirectory;
  if TDirectory.Exists(path) then begin
    lFiles := TDirectory.GetFiles(path,'*.pas', TSearchOption.soAllDirectories) ;
    for filepath in lFiles do
      if TPath.GetFileNameWithoutExtension(filepath).ToLower = aUnitName.ToLower then begin
        Result := True;
        FProject.AddFile(filepath, True);
        Break;
      end;
  end;
end;

procedure TulProjectWatcher.UnregisterFromProject;
begin
  FProject.RemoveNotifier(FIndex);
  FProject := nil;
end;

{$endregion}

{$region 'TUnitSearch'}

procedure TUnitSearch.AfterConstruction;
begin
  inherited;
  AddDefine('VER280');
  AddDefine('VER270');
  AddDefine('VER260');
  AddDefine('MSWINDOWS');
  AddDefine('Android');
  AddDefine('iOS');
  AddDefine('POSIX');
  OnMessage := ErrorOccurred;
  InterfaceOnly := False;
  fUnits := TList<string>.Create;
end;

procedure TUnitSearch.BeforeDestruction;
begin
  fUnits.Free;
  inherited;
end;

procedure TUnitSearch.ClearUnits;
begin
  fUnits.Clear;
end;

procedure TUnitSearch.ErrorOccurred(Sender: TObject;
  const Typ: TMessageEventType; const Msg: string; X, Y: Integer);
var
  lMessage: string;
begin
  case Typ of
    meError: lMessage := 'Error: '+ Msg;
    meNotSupported: lMessage := 'Not Supported: '+ Msg;
  end;
  OutputDebugString(PChar(lMessage));
end;

function TUnitSearch.FoundUnits: TArray<string>;
begin
  Result := fUnits.ToArray;
end;

procedure TUnitSearch.UsedUnitName;
var
  lContent: string;
begin
  lContent := '';
  lContent := lContent + Lexer.Token;
  Expected(ptIdentifier);
  while (TokenID <> ptComma) and (TokenID <> ptSemiColon) do
  begin
    lContent := lContent + Lexer.Token;
    NextToken;
    lContent := lContent + Lexer.Token;
    Expected(ptIdentifier);
  end;
  fUnits.Add(lContent);
end;

{$endregion}

{ TulConfig }

procedure TulConfig.BeforeDestruction;
begin
  inherited;

end;

{ TulConfigurator }

procedure TulConfigurator.Load;
var
  lAppDataPath: string;
  lIni: TIniFile;
  lWatcher: IulIDEWatcher;
begin
  if Supports(IDEWatcher, IulIDEWatcher, lWatcher)  then begin
    lAppDataPath := TPath.GetHomePath + TPath.DirectorySeparatorChar + 'UsersLoader';
    lIni := TIniFile.Create(IncludeTrailingPathDelimiter(lAppDataPath)+'config.ini');
    try
      if lIni.SectionExists('Paths') and lIni.ValueExists('Paths','LibraryDirectory') then
        lIni.ReadString('Paths','LibraryDirectory',lWatcher.Config.LibraryDirectory);
      if lIni.SectionExists('Cache') and lIni.ValueExists('Paths','CacheSystemFolders') then
        lIni.ReadBool('Cache','CacheSystemFolders',lWatcher.Config.CacheSystemFolders);
      if lIni.SectionExists('Paths') and lIni.ValueExists('Paths','LibraryUseSubfolders') then
        lIni.ReadBool('Paths','LibraryUseSubfolders',lWatcher.Config.UseSubfolder);
    finally
      lIni.Free;
    end;
  end;
end;

procedure TulConfigurator.Save;
var
  lAppDataPath: string;
  lIni: TIniFile;
  lWatcher: IulIDEWatcher;
begin
  if Supports(IDEWatcher, IulIDEWatcher, lWatcher)  then begin
    lAppDataPath := TPath.GetHomePath + TPath.DirectorySeparatorChar + 'UsersLoader';
    if not TDirectory.Exists(lAppDataPath) then
      TDirectory.CreateDirectory(lAppDataPath);
    lIni := TIniFile.Create(IncludeTrailingPathDelimiter(lAppDataPath)+'config.ini');
    try
      lIni.WriteString('Paths','LibraryDirectory',lWatcher.Config.LibraryDirectory);
      lIni.WriteBool('Cache','CacheSystemFolders',lWatcher.Config.CacheSystemFolders);
      lIni.WriteBool('Paths','LibraryUseSubfolders',lWatcher.Config.UseSubfolder);
      lIni.UpdateFile;
    finally
      lIni.Free;
    end;
  end;
end;

initialization
  Index := -1;
  Index2 := -1;

finalization
  (BorlandIDEServices as IOTAProjectManager).RemoveMenuItemCreatorNotifier(Index2);
  (BorlandIDEServices as IOTAServices).RemoveNotifier(Index);
  IDEWatcher := nil;
end.
